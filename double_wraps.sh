#!/bin/bash


#########################################################

exec 6<&0          # Link file descriptor #6 with stdin.
                   # Saves stdin.
exec 0<&-          # close stdin

#########################################################
# Fixed parameter
OPENSSL="$(which openssl)"
NETCAT="$(which netcat)"
SOCAT="$(which socat)"
CONNECT="$(which connect)"

if [ -z "${OPENSSL+x}" -o ! -x "${OPENSSL}" ]
then
    >&2 echo "Missing OPENSSL command line tool"
    exit -1
fi

#########################################################
# some definitions of usefull functions

function cleanup {

    >&2 echo "Running cleanup"

    if [ -n "${PIPE_S2O}" ]
    then
        rm "${PIPE_S2O}" 2> /dev/null
    fi

    if [ -n "${PIPE_O2S}" ]
    then
        rm "${PIPE_O2S}" 2> /dev/null
    fi
}


function dump_options {

    OPTIONS="-T|--target-address <TARGET_HOST>[:<TARGET_PORT>]"
    OPTIONS="${OPTIONS} [--verify-target]"
    OPTIONS="${OPTIONS} -S|--service-sni <SERVICE_SNI>"
    OPTIONS="${OPTIONS} [--verify-service]"
    OPTIONS="${OPTIONS} [-v|--verbose-verification]"
    OPTIONS="${OPTIONS} [-D|--double-wrap]"
    OPTIONS="${OPTIONS} [-F|--prefix <CONNECTION_PREFIX>]"
    OPTIONS="${OPTIONS} [--cafile <CAFILE>.pem]"
    OPTIONS="${OPTIONS} [-X|--http-proxy <PROXY_HOST>:<PROXY_PORT>]"

    >&2 echo "Usage:"
    >&2 echo "${0} ${OPTIONS}"
}


#########################################################
# Parse parameters

unset IPORT
unset CPORT
unset PROXY
unset CAFILE
unset TARGET_ADDRESS
unset TARGET_SNI
unset VERIFY_TARGET
unset VERIFY_SERVICE
unset VERBOSE_VERIFY
unset DOUBLEWRAP
unset CONNECTION_PREFIX


POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="${1}"

    case $key in
        -h|--help)
        dump_options
        exit 0
        ;;
        -D|--double-wrap)
        DOUBLEWRAP="YES"
        shift # past argument
        ;;
        -F|--prefix)
        CONNECTION_PREFIX="$2"
        shift # past argument
        shift # past value
        ;;
        -P|--internally-used-proxy-port)
        IPORT="$2"
        shift # past argument
        shift # past value
        ;;
        -C|--internally-used-crosstie-port)
        CPORT="$2"
        shift # past argument
        shift # past value
        ;;
        -X|--http-proxy)
        PROXY="$2"
        shift # past argument
        shift # past value
        ;;
        --cafile)
        CAFILE="$2"
        shift # past argument
        shift # past value
        ;;
        -T|--target-address)
        TARGET_ADDRESS="$2"
        shift # past argument
        shift # past value
        ;;
        -S|--service-sni)
        TARGET_SNI="$2"
        shift # past argument
        shift # past value
        ;;
        --verify-target)
        VERIFY_TARGET="YES"
        shift # past argument
        ;;
        --verify-service)
        VERIFY_SERVICE="YES"
        shift # past argument
        ;;
        -v|--verbose-verification)
        VERBOSE_VERIFY="YES"
        shift # past argument
        ;;
        *)    # unknown option
        >&2 echo "Unknown parameter ${1}"
        dump_options
        exit -1
        shift # past argument
        ;;
    esac
done

#########################################################
# Post process argunments


if [ -z "${TARGET_ADDRESS+x}" ]
then  # no target address provided
    >&2 echo "Missing target address"
    dump_options
    exit -1
else
    TARGET_PORT=${TARGET_ADDRESS/*\:}
    if [ "${TARGET_PORT}" == "${TARGET_ADDRESS}" ]
    then  # no port givin
        TARGET_PORT=443
        TARGET_HOST=${TARGET_ADDRESS}
    else
        TARGET_HOST=${TARGET_ADDRESS/:*}
    fi
fi

if [ -z "${TARGET_SNI+x}" ]
then  # no target address provided

    if [ -n "${DOUBLEWRAP+x}" ]
    then
        >&2 echo "Missing target service SNI for SSL double wrap - need to be different from target hostname"
    
        dump_options
        exit -1
    fi

    # otherwise we set TARGET_SNI equal to TARGET_HOST
    TARGET_SNI="${TARGET_HOST}"
fi


if [ -n "${CAFILE+x}" ]
then
    if [ ! -f ${CAFILE} ]
    then
        >&2 echo "Error reading CAFile ${CAFILE}"
        exit -1
    fi

    CAFILE="-CAfile ${CAFILE}"
fi


if [ -n "${VERIFY_TARGET+x}" ]
then  # verification desired
    SSL1_TARGET_PARAM="${CAFILE} -verify 3 -verify_return_error"
fi

if [ -z "${VERBOSE_VERIFY+x}" ]
then  # verification desired
    SSL_PARAM="-verify_quiet -quiet"
else
    SSL_PARAM="-brief -ign_eof"
fi

# do not use unsecure SSl
SSL_PARAM="${SSL_PARAM} -no_ssl3 -no_tls1 -no_tls1_1 -no_comp"


#########################################################
#  Handling proxy connection

if [ ! -z "${PROXY+x}" ]
then

    if [ -z "${IPORT+x}" ]
    then  # no port provided
        IPORT=$(( ($$ % 64500) + 1024 ))
    fi

    #>&2 echo "Using http proxy ${PROXY} via internal port ${IPORT} for connection to ${TARGET_HOST}:${TARGET_PORT}"

    if [ ! -z "${CONNECT}" -a -x "${CONNECT}" ]
    then
        #>&2 echo "${CONNECT} -p ${IPORT} -H ${PROXY} ${TARGET_HOST} ${TARGET_PORT} > /dev/null"
        $(${CONNECT} -p ${IPORT} -H ${PROXY} ${TARGET_HOST} ${TARGET_PORT} > /dev/null) &

    elif [ ! -z "${SOCAT}" -a -x "${SOCAT}" ]
    then
        #>&2 echo "${SOCAT} TCP4-LISTEN:${IPORT},bind=localhost PROXY:${PROXY/:*/}:${TARGET_HOST}:${TARGET_PORT},proxyport=${PROXY/*:/}"
        ${SOCAT} TCP4-LISTEN:${IPORT},bind=localhost PROXY:${PROXY/:*/}:${TARGET_HOST}:${TARGET_PORT},proxyport=${PROXY/*:/} &
    else
        # TODO: there are plenty of other tools for this task: corkscrew, bash-tcp, ...netcat?
        >&2 echo "Missing proxy relay tool like connect or socat"
        exit -1
    fi

    sleep 0.3s

    # Setting connection parameters for next tool
    SSL1_HOST=localhost
    SSL1_PORT=${IPORT}
else
    SSL1_HOST=${TARGET_HOST}
    SSL1_PORT=${TARGET_PORT}
fi

# check for double wrap mode
if [ -z "${DOUBLEWRAP+x}" ]
then  # No double wrap mode

    OPENSSL_COMMAND="${OPENSSL} s_client ${SSL_PARAM} -connect ${SSL1_HOST}:${SSL1_PORT} -servername ${TARGET_SNI} ${SSL1_TARGET_PARAM}"
    
    if [ -z "${CONNECTION_PREFIX+x}" ]
    then
        #>&2 echo "${OPENSSL_COMMAND}"
        ${OPENSSL_COMMAND} <&6
    else
        # Connection with sending prefix
        { echo "${CONNECTION_PREFIX}"; cat <&6; } | ${OPENSSL_COMMAND}
    fi
else # double wrap mode

    #########################################################
    # Preparing names pipes

    trap cleanup EXIT

    # stream from SSL to CROSSTIE

    PIPE_S2O=$(mktemp -u)
    mkfifo ${PIPE_S2O}
    # attach it to file descriptor
    exec 7<>${PIPE_S2O}
    # unlink the named pipe
    #rm ${PIPE_S2O}
    #unset PIPE_S2O

    #########################################
    # stream from CROSSTIE to SSL

    PIPE_O2S=$(mktemp -u)
    mkfifo ${PIPE_O2S}
    # attach it to file descriptor
    exec 8<>${PIPE_O2S}
    # unlink the named pipe
    #rm ${PIPE_O2S}
    #unset PIPE_O2S

    #########################################################
    # Opening first SSL layer

    #>&2 echo "${OPENSSL} s_client ${SSL_PARAM} -connect ${SSL1_HOST}:${SSL1_PORT} -servername ${TARGET_HOST} ${SSL1_TARGET_PARAM}"
    ${OPENSSL} s_client ${SSL_PARAM} -connect ${SSL1_HOST}:${SSL1_PORT} -servername ${TARGET_HOST} ${SSL1_TARGET_PARAM} <&7 >&8 &

    sleep 0.3s

    #########################################################
    # Starting cross tie

    if [ -z "${CPORT+x}" ]
    then  # no port provided
        if [ -n "${IPORT+x}" ]
        then
            CPORT=$((IPORT+1))
        else
            CPORT=$(( ($$ % 64500) + 1024 ))
        fi
    fi

    if [ ! -z "${NETCAT}" -a -x "${NETCAT}" ]
    then
        #>&2 echo "Cross tie using netcat via port ${CPORT}"

        #>&2 echo "${NETCAT} -l localhost ${CPORT}"
        ${NETCAT} -l localhost ${CPORT} <&8 >&7 &
        sleep 0.3s
    elif [ ! -z "${SOCAT}" -a -x "${SOCAT}" ]
    then
        #>&2 echo "Cross tie using socat via port ${CPORT}"
        
        #>&2 echo "${SOCAT} - TCP4-LISTEN:${CPORT},bind=localhost"
        ${SOCAT} - TCP4-LISTEN:${CPORT},bind=localhost <&8 >&7 &
        sleep 0.3s
    else
        >&2 echo "Mising cross tie tool (netcat or socat)"
        exit -1
    fi

    SSL2_HOST=localhost
    SSL2_PORT=${CPORT}

    #########################################################
    # Opening second SSL layer

    if [ ! -z "${VERIFY_SERVICE+x}" ]
    then  # verification desired
        SSL2_TARGET_PARAM="${CAFILE} -verify 3 -verify_return_error"
    fi

    #>&2 echo "${OPENSSL} s_client ${SSL_PARAM} -connect ${SSL2_HOST}:${SSL2_PORT} -servername ${TARGET_SNI} ${SSL2_TARGET_PARAM}"
    ${OPENSSL} s_client ${SSL_PARAM} -connect ${SSL2_HOST}:${SSL2_PORT} -servername ${TARGET_SNI} ${SSL2_TARGET_PARAM} <&6 

    #########################################################
    ## finish

    exec 7<&-
    exec 8<&-

    rm ${PIPE_S2O}
    rm ${PIPE_O2S}
fi

exec 6<&-

exit 0


